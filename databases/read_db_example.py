import mysql.connector
from mysql.connector import errorcode

# Set table names:
myTable="Monitor_Run_Atmosphere"


#Set the connections parameters
config = {
  'user': 'user',
  'password': 'password',
  'host': 'host',
  'database': 'myDataBase',
  'port': 3306,
  'raise_on_warnings': True,
}

#try connection to db
try:
  cnx = mysql.connector.connect(**config)
  print "##################################################"
  print("You are now connected")
  print "--------------------------------------------------"
  cursor=cnx.cursor()
except mysql.connector.Error as err:
  if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
    print("Something is wrong with your user name or password \n")
  elif err.errno == errorcode.ER_BAD_DB_ERROR:
    print("Database does not exist \n")
  else:
    print(err)

#Retrieve some values from a table
cursor.execute("""select Run, TransparencyCoefficient_mean from Monitor_Run_Atmosphere where Run=%i limit 1 offset 1;"""%(run))
for data in cursor:
    print data