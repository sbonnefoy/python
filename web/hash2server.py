#This scripts retrieves a string from a web page, 
#compute the md5 hash of the string, and submits 
#the hash back to the web server. This was part of a challenge

from bs4 import BeautifulSoup as bs
import requests
import md5
import hashlib
import urllib

#send a get request to the url
target_url = "http://MyWeb.com" 
session = requests.Session()
response = session.get(target_url)
page = response.text
print(page)
print('\n\n\n\n')

#extracting the value of the string
soup = bs(page, 'html.parser')
s = soup.find('h3')
value = s.string.extract()
#print(value)

#computing the md5 digest of the string
md5_val = hashlib.md5(value).hexdigest()
#print(md5_val)

#submitting to the form
post_tag ={'hash':str(md5_val)} 
#post_tag = urllib.urlencode(post_tag)
print('submitting the following data:')
print(post_tag)
print('\n\n')

post_response = session.post(target_url, data=post_tag)
print(post_response.text)
